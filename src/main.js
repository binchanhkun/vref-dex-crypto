import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.config.productionTip = false

import VueMaterialTabs from "vue-material-tabs";

Vue.use(VueMaterialTabs);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
